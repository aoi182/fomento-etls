﻿using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Text;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Test.Fomento
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void InvokeSp()
        {
            StringBuilder stringBuilder = new StringBuilder();

            SqlConnection cnx = new SqlConnection(ConfigurationManager.ConnectionStrings["SGE"].ToString());
            cnx.InfoMessage += (sender, info) =>
            {
                stringBuilder.AppendLine(info.Message);
            };
            
            SqlCommand cmd = new SqlCommand
            {
                Connection = cnx,
                CommandTimeout = 10000,
                CommandType = CommandType.StoredProcedure,
                CommandText = "myfuncion_sp"
            };
            cmd.Parameters.Add("@p1", SqlDbType.Text);
            cmd.Parameters["@p1"].Value = "aoi";
            cmd.Connection.Open();

            var trn = cnx.BeginTransaction();

            try
            {
                cmd.Transaction = trn;
                cmd.ExecuteNonQuery();
                trn.Commit();
            }
            catch (Exception ex)
            {
                string msg = ex.Message + (ex.InnerException == null ? "" : ex.InnerException.Message);
                File.WriteAllText(@"error.log", msg);
                trn.Rollback();
            }
            finally
            {
                cmd.Connection.Close();
                File.WriteAllText(@"messages.log", stringBuilder.ToString());
            }
        }
    }
}