﻿using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Test.Fomento
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void InvokeSp()
        {
            StringBuilder stringBuilder = new StringBuilder();

            SqlConnection cnx = new SqlConnection(ConfigurationManager.ConnectionStrings["SGE"].ToString());
            cnx.InfoMessage += (sender, info) =>
            {
                stringBuilder.AppendLine(info.Message);
            };
            
            SqlCommand cmd = new SqlCommand
            {
                Connection = cnx,
                CommandTimeout = 10000,
                CommandType = CommandType.StoredProcedure,
                CommandText = "myfuncion_sp"
            };
            cmd.Parameters.Add("@p1", SqlDbType.Text);
            cmd.Parameters["@p1"].Value = "aoi";
            cmd.Connection.Open();

            var trn = cnx.BeginTransaction();

            try
            {
                cmd.Transaction = trn;
                cmd.ExecuteNonQuery();
                trn.Commit();
            }
            catch (Exception ex)
            {
                string msg = ex.Message + (ex.InnerException == null ? "" : ex.InnerException.Message);
                File.WriteAllText(@"error.log", msg);
                trn.Rollback();
            }
            finally
            {
                cmd.Connection.Close();
                File.WriteAllText(@"messages.log", stringBuilder.ToString());
            }
        }

        [TestMethod]
        public void ValidateEmailTest()
        {
            var emailAddress = "aoi.182@live.";
            string emailAddressPattern = @"^(([^<>()[\]\\.,;:\s@\""]+"
                        + @"(\.[^<>()[\]\\.,;:\s@\""]+)*)|(\"".+\""))@"
                        + @"((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}"
                        + @"\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+"
                        + @"[a-zA-Z]{2,}))$";
            // Create a regex object with the pattern
            Regex emailAddressRegex = new Regex(emailAddressPattern);
            // Check if it is match and return that value (boolean)
            var b = emailAddressRegex.IsMatch(emailAddress);

            Assert.IsTrue(b);
        }

        [TestMethod]
        public void Validatedate()
        {
            DateTime dateTime;
            var b2 = DateTime.TryParseExact("15/5/2018", "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out dateTime);
            var b3 = DateTime.TryParseExact("5/11/2018", "d/M/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out dateTime);
            var b4 = DateTime.TryParseExact("15/15/2018", "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out dateTime);
            var b5 = DateTime.TryParseExact("15/05/2018", "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out dateTime);

            var b6 = DateTime.TryParseExact("5/15/2014", "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out dateTime);
        }
    }
}